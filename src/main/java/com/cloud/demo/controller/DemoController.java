package com.cloud.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author: bobi
 * @date: 2019-03-10 11:54
 * @description:
 */
@RestController
public class DemoController {

    @GetMapping("demo1")
    public ModelAndView demo1() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("demo1");
        return modelAndView;
    }

    @GetMapping("demo2")
    public ModelAndView demo2() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("demo2");
        return modelAndView;
    }
}
